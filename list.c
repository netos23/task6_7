//
// Created by nikmo on 06.06.2021.
//

#include <malloc.h>
#include <stdio.h>
#include "include/list.h"

#pragma pack(push, 1)
struct List {
	list_node_t *head;
	list_node_t *tail;
	size_t size;
	size_t sizeOfElements;
};

struct ListNode {
	void *val;
	list_node_t *next;
};
#pragma pack(pop)


list_t *create_list() {
	list_t *pList = (list_t *) malloc(sizeof(list_t));

	pList->size = 0;
	pList->head = NULL;
	pList->tail = NULL;

	return pList;
}

bool appenli(list_t *list, void *item) {
	list_node_t *pListNode = (list_node_t *) malloc(sizeof(list_node_t));
	pListNode->next = NULL;
	pListNode->val = item;

	if (list->head == NULL) {
		list->head = list->tail = pListNode;
	} else {
		list->tail->next = pListNode;
		list->tail = list->tail->next;
	}

	list->size++;

	return true;
}

bool delli(list_t *list, size_t index) {
	if (list->size <= index) {
		return false;
	}
	list_node_t *pNode;
	if (index == 0) {
		pNode = list->head;
		list->head = pNode->next;
		free(pNode);

		if (--list->size <= 1) {
			list->tail = list->head;
		}
	} else {
		size_t i = 0;
		list_node_t *cur = list->head;

		while (index - 1 != i++) {
			cur = cur->next;
		}

		pNode = cur->next;
		cur->next = pNode->next;
		free(pNode);
	}


	return true;
}

void *get(list_t *list, size_t index) {
	if (list->size <= index) {
		return NULL;
	}

	size_t i = 0;
	list_node_t *cur = list->head;
	while (index != i++) {
		cur = cur->next;
	}

	return cur->val;
}

void foreach(list_t *list, void (*consumer)(void *, int)) {

	size_t i = 0;
	list_node_t *cur = list->head;
	while (list->size != i) {
		consumer(cur->val, i++);
		cur = cur->next;
	}
}

size_t size(list_t *list) {
	return list->size;
}

void delete_list(list_t *list) {
	for (int i = 0; i < list->size; i++) {
		delli(list, i);
	}
	free(list);
}