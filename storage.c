//
// Created by nikmo on 06.06.2021.
//
#include <malloc.h>
#include <strings.h>
#include <stdio.h>
#include "include/storage.h"


#pragma pack(push, 1)
struct Vehicle {
	int enginePower;
	char *carBrand;
	bool isNew;
};

struct Record {
	id_t id;
	vehicle_type_t vehicleType;
	vehicle_t *base;

	union {
		struct {
			char *bikeType;
			bool hasMotorcycleSidecar;
		} bike;

		struct {
			int trunkVolume;
			bool hasTrailer;
		} car;

		struct {
			int countPassengers;
		} bus;

		struct {
			char *bodyworkType;
			int loadCapacity;
		} truck;
	};
};


#pragma pack(pop)

static int storageSequence = 0;


vehicle_t *create_vehicle(int enginePower, char *brand, bool isNew) {
	vehicle_t *pVehicle = (vehicle_t *) malloc(sizeof(vehicle_t));

	pVehicle->enginePower = enginePower;
	pVehicle->carBrand = (char *) calloc(strlen(brand) + 1, sizeof(char));
	strcpy(pVehicle->carBrand, brand);
	pVehicle->isNew = isNew;

	return pVehicle;
}

record_t *create_base_record(vehicle_t *base) {
	record_t *pRecord = (record_t *) malloc(sizeof(record_t));
	pRecord->base = base;
	pRecord->id = storageSequence++;
	return pRecord;
}

record_t *create_bike(vehicle_t *base, char *bikeType, bool hasMotorcycleSidecar) {
	record_t *pRecord = create_base_record(base);

	pRecord->vehicleType = BIKE;
	pRecord->bike.bikeType = (char *) calloc(strlen(bikeType) + 1, sizeof(char));
	strcpy(pRecord->bike.bikeType, bikeType);
	pRecord->bike.hasMotorcycleSidecar = hasMotorcycleSidecar;

	return pRecord;
}

record_t *create_car(vehicle_t *base, int trunkVolume, bool hasTrailer) {
	record_t *pRecord = create_base_record(base);

	pRecord->vehicleType = CAR;
	pRecord->car.trunkVolume = trunkVolume;
	pRecord->car.hasTrailer = hasTrailer;

	return pRecord;
}

record_t *create_bus(vehicle_t *base, int countPassengers) {
	record_t *pRecord = create_base_record(base);

	pRecord->vehicleType = BUS;
	pRecord->bus.countPassengers = countPassengers;

	return pRecord;
}

record_t *create_truck(vehicle_t *base, char *bodyworkType, int loadCapacity) {
	record_t *pRecord = create_base_record(base);

	pRecord->vehicleType = TRUCK;
	pRecord->truck.bodyworkType = (char *) calloc(strlen(bodyworkType) + 1, sizeof(char));
	strcpy(pRecord->truck.bodyworkType, bodyworkType);
	pRecord->truck.loadCapacity = loadCapacity;

	return pRecord;
}


void delete_vehicle(vehicle_t *vehicle) {
	free(vehicle->carBrand);
	free(vehicle);
}

void delete_record(record_t *record) {
	delete_vehicle(record->base);

	switch (record->vehicleType) {
		case BIKE:
			free(record->bike.bikeType);
			break;
		case TRUCK:
			free(record->truck.bodyworkType);
			break;
		default:
			break;
	}

	free(record);
}

void fprint_record(FILE *restrict file, record_t *record, char *delimiter) {
	vehicle_t *pBase = record->base;

	_fprintf_p(file, "%2$zu%1$s%3$d%1$s%4$s%1$s%5$d%1$s%6$d",
			   delimiter,
			   record->id,
			   record->vehicleType,
			   pBase->carBrand,
			   pBase->enginePower,
			   pBase->isNew);

	switch (record->vehicleType) {
		case BIKE:
			_fprintf_p(file, "%1$s%2$s%1$s%3$d\n",
					   delimiter,
					   record->bike.bikeType,
					   record->bike.hasMotorcycleSidecar);
			break;
		case CAR:
			_fprintf_p(file, "%1$s%2$d%1$s%3$d\n",
					   delimiter,
					   record->car.trunkVolume,
					   record->car.hasTrailer);
			break;
		case BUS:
			_fprintf_p(file, "%1$s%2$d\n",
					   delimiter,
					   record->bus.countPassengers);
			break;
		case TRUCK:
			_fprintf_p(file, "%1$s%2$s%1$s%3$d\n",
					   delimiter,
					   record->truck.bodyworkType,
					   record->truck.loadCapacity);
			break;
		default:
			break;
	}

}

void print_record(record_t *record, char *delimiter) {
	vehicle_t *pBase = record->base;

	_printf_p("%2$-5zu%1$s%3$-5d%1$s%4$-12s%1$s%5$-12d%1$s%6$-6d",
			  delimiter,
			  record->id,
			  record->vehicleType,
			  pBase->carBrand,
			  pBase->enginePower,
			  pBase->isNew);

	switch (record->vehicleType) {
		case BIKE:
			_printf_p("%1$s%2$s%1$s%3$d\n",
					  delimiter,
					  record->bike.bikeType,
					  record->bike.hasMotorcycleSidecar);
			break;
		case CAR:
			_printf_p("%1$s%2$d%1$s%3$d\n",
					  delimiter,
					  record->car.trunkVolume,
					  record->car.hasTrailer);
			break;
		case BUS:
			_printf_p("%1$s%2$d\n",
					  delimiter,
					  record->bus.countPassengers);
			break;
		case TRUCK:
			_printf_p("%1$s%2$s%1$s%3$d\n",
					  delimiter,
					  record->truck.bodyworkType,
					  record->truck.loadCapacity);
			break;
		default:
			break;
	}

}

record_t *fscan_record(FILE *restrict file) {
	id_t id = 0;
	vehicle_type_t vehicleType = BIKE;
	char textBuffer[MAX_BUFFER_LEN];
	int intBuffer = 0;
	bool boolBuffer = false;

	fscanf(file, "%zu %d %s %d %d", &id, &vehicleType, textBuffer, &intBuffer, &boolBuffer);

	vehicle_t *pVehicle = create_vehicle(intBuffer, textBuffer, boolBuffer);
	record_t *pRecord = NULL;

	switch (vehicleType) {
		case BIKE:
			fscanf(file, "%s %d", textBuffer, &boolBuffer);
			pRecord = create_bike(pVehicle, textBuffer, boolBuffer);
			break;
		case CAR:
			fscanf(file, "%d %d", &intBuffer, &boolBuffer);
			pRecord = create_car(pVehicle, intBuffer, boolBuffer);
			break;
		case BUS:
			fscanf(file, "%d", &intBuffer);
			pRecord = create_bus(pVehicle, intBuffer);
			break;
		case TRUCK:
			fscanf(file, "%s %d", textBuffer, &intBuffer);
			pRecord = create_truck(pVehicle, textBuffer, intBuffer);
			break;
		default:
			delete_vehicle(pVehicle);
			return NULL;
	}

	pRecord->id = id;

	return pRecord;
}

size_t sizeof_vehicle_t() {
	return sizeof(vehicle_t);
}

size_t sizeof_record_t() {
	return sizeof(record_t);
}
