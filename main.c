#include <stdio.h>
#include "include/array_storage.h"
#include "include/list.h"

#define PATH_SIZE 300

#define SUCCESS "SUCCESS\n"
#define FAIL "FAIL\n"
typedef enum Chose {
	SAVE = 1,
	LOAD,
	ADD,
	DELETE,
	PRINT,
	CREATE
} chose_t;

int main() {

	chose_t chose = 0;
	char pathBuffer[PATH_SIZE], inputBuffer[MAX_BUFFER_LEN];
	vehicle_type_t vehicleType = 0;
	int tmp0 = 0, tmp1 = 0;
	bool tmpBool = false;
	storage_t *pStorage = NULL;
	vehicle_t *pVehicle = NULL;
	record_t *pRecord = NULL;
	bool res = false;

	while (true) {
		printf("\nMENU:\nany - quit\n1 - save\n2 - load\n3 - add\n4 - delete record\n5 - print\n6 - create\n");
		scanf("%d", &chose);

		switch (chose) {
			case SAVE:
				if (pStorage == NULL) {
					printf("Missing storage\n");
					break;
				}

				printf("Enter path:\n>");
				scanf("%s", pathBuffer);
				res = save_storage(pStorage, pathBuffer);

				printf(res ? SUCCESS : FAIL);
				break;
			case LOAD:
				if (pStorage != NULL) {
					printf("WARNING!!!\nYour data will lost!\n\n");
					delete_storage(pStorage);
				}

				printf("Enter path:\n>");
				scanf("%s", pathBuffer);
				pStorage = load_storage(pathBuffer);

				printf(pStorage != NULL ? SUCCESS : FAIL);
				break;
			case ADD:
				if (pStorage == NULL) {
					printf("Missing storage\n");
					break;
				}

				printf("Enter type:\n0 - BIKE\n1 - CAR\n2 - BUS\n3 - TRUCK\n>");
				scanf("%d", &vehicleType);

				printf("Enter car mark:\n>");
				scanf("%s", inputBuffer);

				printf("Enter engine power:\n>");
				scanf("%d", &tmp0);

				printf("Is it new car? \n0 - n \nany - y\n>");
				scanf("%d", &tmpBool);

				pVehicle = create_vehicle(tmp0, inputBuffer, tmpBool);

				switch (vehicleType) {
					case BIKE:
						printf("Enter bike type:\n>");
						scanf("%s", inputBuffer);

						printf("Has it hasMotorcycleSidecar? \n0 - n \nany - y\n>");
						scanf("%d", &tmpBool);

						pRecord = create_bike(pVehicle, inputBuffer, tmpBool);
						break;
					case CAR:
						printf("Enter trunk volume:\n>");
						scanf("%d", &tmp0);

						printf("Has it Trailer? \n0 - n \nany - y\n>");
						scanf("%d", &tmpBool);
						pRecord = create_car(pVehicle, tmp0, tmpBool);
						break;
					case BUS:
						printf("Enter passages count:\n>");
						scanf("%d", &tmp0);

						pRecord = create_bus(pVehicle, tmp0);
						break;
					case TRUCK:
						printf("Enter bodywork type:\n>");
						scanf("%s", inputBuffer);

						printf("Enter load capacity:\n>");
						scanf("%d", &tmp0);

						pRecord = create_truck(pVehicle, inputBuffer, tmp0);
						break;
					default:
						delete_vehicle(pVehicle);
						pVehicle = NULL;
						printf("Error occurred!\n");
						break;
				}
				if (pVehicle != NULL) {
					res = add(pStorage, pRecord);
					if (res) {
						printf("ID    TYPE  CAR_BRAND    ENGINE_POWER IS_NEW SPECIALS\n");
						print_record(pRecord, " ");
					}
					printf(res ? SUCCESS : FAIL);
				} else {
					printf(FAIL);
				}
				break;

			case DELETE:
				if (pStorage == NULL) {
					printf("Missing storage\n");
					break;
				}

				printf("Enter id:\n>");
				scanf("%d", &tmp0);
				res = delete(pStorage, tmp0);
				printf(res ? SUCCESS : FAIL);
				break;

			case PRINT:
				if (pStorage == NULL) {
					printf("Missing storage\n");
					break;
				}

				print_storage(pStorage, 0, 0);
				break;

			case CREATE:
				if (pStorage != NULL) {
					printf("WARNING!!!\nYour data will lost!\n\n");
					delete_storage(pStorage);
				}
				
				#ifdef TASK6_7_ARRAY_STORAGE_H
				printf("Enter size:\n>");
				scanf("%d", &tmp0);
				pStorage = create_storage(tmp0);
				#endif
				
				#ifdef TASK6_7_LIST_STORAGE_H
				pStorage = create_storage();
				#endif
				break;

			default:
				if (pStorage != NULL) {
					delete_storage(pStorage);
				}
				return 0;
		}
	}
}
