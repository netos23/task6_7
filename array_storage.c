//
// Created by nikmo on 06.06.2021.
//
#include <malloc.h>
#include "include/array_storage.h"


#pragma pack(push, 1)
struct Storage {
	record_t **records;
	id_t registered;
	size_t size;
};
#pragma pack(pop)

storage_t *create_storage(size_t size) {
	storage_t *pStorage = (storage_t *) malloc(sizeof(storage_t));

	pStorage->registered = 0;
	pStorage->size = size;
	pStorage->records = (record_t **) calloc(size, sizeof(record_t **));

	return pStorage;
}

storage_t *load_storage(const char *filename) {
	FILE *file = fopen(filename, "r");

	if (file == NULL) {
		fclose(file);
		return NULL;
	}

	id_t id = 0;
	size_t size = 0;
	fscanf(file, "%zu %zu", &size, &id);

	storage_t *storage = create_storage(size);
	for (int i = 0; i < id; i++) {
		record_t *pRecord = fscan_record(file);
		if (pRecord != NULL) {
			add(storage, pRecord);
		}
	}

	fclose(file);
	return storage;
}

bool save_storage(storage_t *storage, const char *filename) {
	FILE *file = fopen(filename, "w");

	if (file == NULL) {
		fclose(file);
		return false;
	}

	fprintf(file, "%zu %zu\n", storage->size, storage->registered);
	for (int i = 0; i < storage->registered; i++) {
		record_t *pRecord = storage->records[i];
		fprint_record(file, pRecord, DELIMITER);
	}

	fclose(file);
	return true;
}

bool add(storage_t *storage, record_t *item) {
	if (storage->registered >= storage->size) {
		return false;
	}
	storage->records[storage->registered++] = item;
	return true;
}

bool delete(storage_t *storage, id_t id) {
	if (storage->registered >= storage->size) {
		return false;
	}

	storage->size--;
	delete_record(storage->records[id]);
	for (int i = id; i < storage->size; i++) {
		storage->records[i] = storage->records[i + 1];
	}

	return true;
}

void print_storage(storage_t *storage, size_t offset, size_t limit) {
	if (offset + limit >= storage->registered) {
		printf("Wrong params");
		return;
	}

	printf("ID    TYPE  CAR_BRAND    ENGINE_POWER IS_NEW SPECIALS\n");
	for (int i = offset; i < storage->registered; i++) {
		print_record(storage->records[i], " ");
	}
}

void delete_storage(storage_t *storage) {
	for (int i = 0; i < storage->size; i++) {
		delete_record(storage->records[i]);
	}
	free(storage->records);
	free(storage);
}

size_t sizeof_storage_t() {
	return sizeof(storage_t);
}
