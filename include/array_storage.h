//
// Created by nikmo on 06.06.2021.
//

#ifndef TASK6_7_ARRAY_STORAGE_H
#define TASK6_7_ARRAY_STORAGE_H

#include "storage.h"

storage_t *create_storage(size_t size);

#endif //TASK6_7_ARRAY_STORAGE_H
