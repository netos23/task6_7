//
// Created by nikmo on 06.06.2021.
//

#ifndef TASK6_7_STORAGE_H
#define TASK6_7_STORAGE_H

#include <stdio.h>
#include <stdbool.h>
#include <crtdefs.h>

#define MAX_BUFFER_LEN 12
#define DELIMITER " "

typedef enum VehicleType {
	BIKE,
	CAR,
	BUS,
	TRUCK
} vehicle_type_t;

typedef struct Vehicle vehicle_t;
typedef struct Storage storage_t;
typedef struct Record record_t;

typedef size_t id_t;

// Методы для создания и удаления новых записей

vehicle_t *create_vehicle(int enginePower, char *brand, bool isNew);

record_t *create_base_record(vehicle_t *base);

record_t *create_bike(vehicle_t *base, char *bikeType, bool hasMotorcycleSidecar);

record_t *create_car(vehicle_t *base, int trunkVolume, bool hasTrailer);

record_t *create_bus(vehicle_t *base, int countPassengers);

record_t *create_truck(vehicle_t *base, char *bodyworkType, int loadCapacity);

void delete_vehicle(vehicle_t *vehicle);

void delete_record(record_t *record);

void fprint_record(FILE *restrict file, record_t *record, char *delimiter);

void print_record(record_t *record, char *delimiter);

record_t *fscan_record(FILE *restrict file);

// Методы для управления контентом хранилища

storage_t *load_storage(const char *filename);

bool save_storage(storage_t *storage, const char *filename);

bool add(storage_t *storage, record_t *item);

bool delete(storage_t *storage, id_t id);

void print_storage(storage_t *storage, size_t offset, size_t limit);

void delete_storage(storage_t *storage);

size_t sizeof_vehicle_t();

size_t sizeof_record_t();

size_t sizeof_storage_t();

#endif //TASK6_7_STORAGE_H
