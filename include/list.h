//
// Created by nikmo on 06.06.2021.
//

#ifndef TASK6_7_LIST_H
#define TASK6_7_LIST_H

#include <crtdefs.h>
#include <stdbool.h>

typedef struct List list_t;
typedef struct ListNode list_node_t;

list_t *create_list();

bool appenli(list_t *list, void *item);

bool delli(list_t *list, size_t index);

void *get(list_t *list, size_t index);

void foreach(list_t *list, void (*consumer)(void *, int));

size_t size(list_t *list);

void delete_list(list_t *list);


#endif //TASK6_7_LIST_H
